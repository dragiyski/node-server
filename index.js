(function () {
    'use strict';

    const events = require('events');
    const fs = require('fs');
    const net = require('net');
    const path = require('path');
    const url = require('url');
    const util = require('util');

    const symbols = {
        alias: Symbol('alias'),
        options: Symbol('options'),
        handlers: Symbol('handlers'),
        id: Symbol('id')
    };

    const defaultOptions = {
        indexFiles: ['index.htm', 'index.html'],
        mimeTypes: {
            '.css': 'text/css',
            '.eot': 'application/vnd.ms-fontobject',
            '.gif': 'image/gif',
            '.html': 'text/html',
            '.ico': 'image/x-icon',
            '.jpg': 'image/jpeg',
            '.jpeg': 'image/jpeg',
            '.js': 'text/javascript',
            '.md': 'text/markdown',
            '.png': 'image/png',
            '.sql': 'text/sql',
            '.svg': 'image/svg+xml',
            '.ttf': 'font/ttf',
            '.txt': 'text/plain',
            '.woff': 'font/woff',
            '.woff2': 'font/woff2',
            '.xml': 'application/xml',
            '.xsd': 'application/xml',
            '.yaml': 'text/yaml',
            '.yml': 'text/yaml'
        }
    };

    class Server extends events.EventEmitter {
        constructor(options = {}) {
            super();
            options = {...defaultOptions, ...this.constructor.defaultOptions, ...options};
            this[symbols.alias] = new Map();
            this[symbols.handlers] = new Map();
            if (typeof options.documentRoot !== 'string' || !path.isAbsolute(options.documentRoot)) {
                throw new TypeError('Argument 1 must be absolute path');
            }
            if (options.indexFiles != null) {
                if (!Array.isArray(options.indexFiles)) {
                    options.indexFiles = [options.indexFiles];
                }
            }
            this[symbols.options] = options;
        }

        execute(request, response) {
            const info = typeof this[symbols.options].info === 'function' ? this[symbols.options].info : () => {};
            const requestUrl = Server.getRequestUrl(request);
            let pathname = this.ensureMinimumLevel(requestUrl.pathname);
            const alias = this.resolveAlias(pathname);
            if (pathname.startsWith('/')) {
                pathname = pathname.substr(1);
            }
            const requestPath = alias != null ? alias.path : path.resolve(this[symbols.options].documentRoot, pathname);
            info(`request to path: ${requestUrl} -> ${requestPath}`);
            this.resolvePath(requestPath, alias).then(location => {
                location.alias = alias;
                location.requestUrl = requestUrl;
                info(`request to file: ${requestUrl} -> ${location.path}`);
                if (location.stats.isDirectory()) {
                    const error = new Error(`Directory without index file: ${location.path}`);
                    error.code = 'EACCES';
                    error.location = location;
                    throw error;
                }
                if (this[symbols.options].showHidden !== true && path.basename(location.path).startsWith('.')) {
                    // Unix-style hidden files are not readable
                    const error = new Error(`Directory without index file: ${location.path}`);
                    error.code = 'EACCES';
                    error.location = location;
                    throw error;
                }
                if (!location.stats.isFile()) {
                    const error = new Error(`Attempt to access non-file: ${location.path}`);
                    error.code = 'EACCESS';
                    error.location = location;
                    throw error;
                }
                if (location.index && location.index.stats && location.index.stats.isDirectory() && !requestUrl.pathname.endsWith('/')) {
                    // HTML pages ignore the last component as a directory if pathname does not ends with slash
                    requestUrl.pathname += '/';
                    response.statusCode = 308;
                    response.setHeader('location', '' + requestUrl);
                    response.end();
                    return;
                }
                const handler = this.resolveHandler(location.path);
                if (typeof handler === 'function') {
                    handler(request, response, location);
                } else {
                    this.defaultHandler(request, response, location);
                }
            }).catch(error => {
                const statusCode = error.code === 'ENOENT' ? 404 : error.code === 'EACCES' ? 403 : 500;
                let hadEvents = false;
                hadEvents = hadEvents || this.emit(`http.error.${statusCode}`, error, request, response);
                hadEvents = hadEvents || this.emit(`http.error`, error, request, response);
                const text = error.stack || error + '';
                if (!hadEvents) {
                    response.statusCode = statusCode;
                    if (!response.headersSent && this[symbols.options].showError) {
                        response.setHeader('content-type', 'text/plain;charset=utf-8');
                        response.setHeader('content-length', Buffer.byteLength(text, 'utf8'));
                        response.end(text, 'utf8');
                    } else {
                        response.end();
                    }
                }
                if (this.listenerCount('error') > 0) {
                    this.emit('error', error, hadEvents);
                } else {
                    process.stderr.write(text + '\n', 'utf8');
                }
            });
        }

        resolveAlias(pathname) {
            for (const [prefix, options] of this[symbols.alias]) {
                if (pathname.startsWith(prefix)) {
                    const suffix = pathname.substr(prefix.length);
                    if (suffix.length === 0 || suffix.startsWith('/')) {
                        return {
                            ...options,
                            ...{
                                prefix,
                                suffix,
                                path: options.documentRoot + suffix
                            }
                        };
                    }
                }
            }
            return null;
        }

        setAlias(prefix, options = {}) {
            options = {...options};
            if (typeof prefix === 'string') {
                if (!prefix.startsWith('/')) {
                    throw new TypeError('Alias prefix must start with / (slash), if string');
                }
            } else {
                throw new TypeError('Alias prefix must be string or RegExp');
            }
            if (typeof options.documentRoot !== 'string' || !path.isAbsolute(options.documentRoot)) {
                throw new TypeError('Alias document root must be absolute path');
            }
            if (options.indexFiles == null) {
                options.indexFiles = this[symbols.options].indexFiles;
            } else if (!Array.isArray(options.indexFiles)) {
                options.indexFiles = [options.indexFiles];
            }
            this[symbols.alias].set(prefix, options);
        }

        clearAlias(pathname = null) {
            if (typeof pathname === 'string') {
                let alias = null;
                while ((alias = this.resolveAlias(pathname)) != null) {
                    this[symbols.alias].delete(alias.prefix);
                }
            } else if (pathname == null) {
                this[symbols.alias].clear();
            } else {
                throw new TypeError('Expected pathname to be string');
            }
        }

        setHandler(match, execute) {
            if (typeof execute !== 'function') {
                throw new TypeError('Expected arguments[1] to be a function');
            }
            if (typeof match !== 'string' && !(match instanceof RegExp)) {
                throw new TypeError('Expected arguments[0] to be a [string] or [object RegExp]');
            }
            this[symbols.handlers].set(match, execute);
        }

        resolveHandler(filePath) {
            for (const [match, execute] of this[symbols.handlers]) {
                if (typeof match === 'string') {
                    if (match === filePath) {
                        return execute;
                    }
                } else if (match instanceof RegExp) {
                    if (match.test(filePath)) {
                        return execute;
                    }
                }
            }
        }

        defaultHandler(request, response, location) {
            const mimeTypes = this[symbols.options].mimeTypes;
            const extname = path.extname(location.path);
            if (Object.hasOwnProperty.call(mimeTypes, extname)) {
                response.setHeader('content-type', mimeTypes[extname]);
            } else {
                response.setHeader('content-type', 'application/octet-stream');
            }
            response.setHeader('content-length', location.stats.size);
            response.setHeader('last-modified', location.stats.mtime.toGMTString());
            response.setHeader('cache-control', 'max-age=0');
            if (typeof request.headers['if-modified-since'] === 'string' && request.headers['if-modified-since'].length > 0) {
                const ifModifiedSince = new Date(request.headers['if-modified-since']);
                if (!isNaN(ifModifiedSince)) {
                    const lastModified = new Date(location.stats.mtime);
                    ifModifiedSince.setMilliseconds(0);
                    lastModified.setMilliseconds(0);
                    if (lastModified <= ifModifiedSince) {
                        response.statusCode = 304;
                        response.end();
                        return;
                    }
                }
            }
            fs.createReadStream(location.path).pipe(response);
        }

        clearHandler(filePath) {
            const toRemove = [];
            for (const match of this[symbols.handlers]) {
                if (typeof match === 'string') {
                    if (match === filePath) {
                        toRemove.push(match);
                    }
                } else if (match instanceof RegExp) {
                    if (match.test(filePath)) {
                        toRemove.push(match);
                    }
                }
            }
            for (const match of toRemove) {
                this[symbols.handlers].delete(match);
            }
            return this;
        }

        async resolvePath(requestPath, alias = null) {
            const fileStats = await util.promisify(fs.stat)(requestPath);
            if (fileStats.isDirectory()) {
                const indexFiles = alias != null && Array.isArray(alias.indexFiles) ? alias.indexFiles : this[symbols.options].indexFiles;
                for (const indexFile of indexFiles) {
                    const indexPath = path.resolve(requestPath, indexFile);
                    try {
                        const indexStats = await util.promisify(fs.stat)(indexPath);
                        if (indexStats.isFile()) {
                            return {
                                request: requestPath,
                                path: indexPath,
                                stats: indexStats,
                                index: {
                                    file: indexFile,
                                    stats: fileStats
                                }
                            };
                        }
                    } catch (e) {
                        if (e.code !== 'ENOENT' && e.code !== 'EACCES') {
                            throw e;
                        }
                    }
                }
            }
            return {
                request: requestPath,
                path: requestPath,
                stats: fileStats
            };
        }

        ensureMinimumLevel(pathname) {
            const directoryStack = [];
            pathname = pathname.split('/');
            while (pathname.length > 0) {
                const entry = pathname.shift();
                if (entry.length <= 0 || entry === '.') {
                    continue;
                }
                if (entry === '..') {
                    directoryStack.pop();
                }
                directoryStack.push(entry);
            }
            return '/' + directoryStack.join('/');
        }

        get documentRoot() {
            return this[symbols.options].documentRoot;
        }
    }

    Server.getRequestUrl = function (request) {
        return new url.URL(request.url, Server.getBaseUrl(request));
    };

    Server.getBaseUrl = function (request) {
        return url.format({
            protocol: request.socket.encrypted ? 'https:' : 'http:',
            slashes: true,
            host: request.authority || request.headers && request.headers.host || (() => {
                const addressUrl = {
                    hostname: Server.getIP(request.socket.localAddress)
                };
                if (request.socket.localPort !== (request.socket.encrypted ? 443 : 80)) {
                    addressUrl.port = request.socket.localPort;
                }
                return url.format(addressUrl);
            })
        });
    };

    Server.getIP = function (ip) {
        if (net.isIPv6(ip)) {
            const address = this.parseIPv6(ip);
            if (address[0] === 0 && address[1] === 0 && address[2] === 0 && address[3] === 0 && address[4] === 0 && address[5] === 65535) {
                // IP is actually IPv4 encoded as IPv6, so we extract it
                return (address[6] >> 8) + '.' + (address[6] & 0xFF) + '.' + (address[7] >> 8) + '.' + (address[7] & 0xFF);
            }
        }
        return ip;
    };

    Server.parseIPv6 = function (ip) {
        ip = ip.split('::');
        let g = 0;
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = ip[i].split(':');
            g += ip[i].length;
        }
        const ip4 = /([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/.exec(ip[ip.length - 1][ip[ip.length - 1].length - 1]);
        if (ip4) {
            ip[ip.length - 1].pop();
            ip[ip.length - 1].push(ip4[1] * 256 + (ip4[2] | 0));
            ip[ip.length - 1].push(ip4[3] * 256 + (ip4[4] | 0));
        }
        if (ip.length === 2 && g < 8) {
            if (ip[0].length === 1 && ip[0][0] === '') {
                ip[0].length = 0;
            }
            ip[2] = ip[1];
            ip[1] = [];
            while (g < 8) {
                ip[1].push(0);
                ++g;
            }
        }
        ip = Array.prototype.concat.apply([], ip);
        if (ip.length !== 8) {
            return null;
        }
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = typeof ip[i] === 'number' ? ip[i] : ip[i].length > 0 ? parseInt(ip[i], 16) : 0;
            if (!isFinite(ip[i])) {
                return null;
            }
        }
        return ip;
    };

    Server.defaultOptions = defaultOptions;

    module.exports = Server;
})();
